const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

/**
 * Create Express server.
 */
const app = express();

/**
 * Connect to MongoDB.
 */
mongoose.Promise = global.Promise;

/**
 * Express configuration.
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// CORS
// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   res.setHeader('Cache-Control', 'no-cache');
//   next();
// });

app.get('/', function(req, res){
    res.send('Hello World')
})

app.listen(3001, function(){
    console.log("app running on port 3001")
});
